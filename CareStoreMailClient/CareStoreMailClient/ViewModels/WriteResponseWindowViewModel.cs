﻿using System;
using GmailLibrary;

namespace CareStoreMailClient.ViewModels
{
    public class WriteResponseWindowViewModel
    {

        #region Members

        private IMailMessage _mailMessage;

        public String Response { get; set; }
       
        public String ShortName
        {
            get
            {
                return GetShortName();
            }
        }

        public IMailMessage MailMessage
        {
            get
            {
                return _mailMessage;
            }
        }
        public String Subject
        {
            get
            {
                return _mailMessage.Subject;
            }
        }


        #endregion

        public WriteResponseWindowViewModel(IMailMessage msg)
        {
            _mailMessage = msg;
        }

        private string GetShortName()
        {
            var strings = _mailMessage.From.Split('<');

            return strings[0];
        }       
    }
}
