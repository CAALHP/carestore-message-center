﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;
using CAALHP.SOA.ICE.ClientAdapters;
using CareStoreMailClient.Annotations;
using CareStoreMailClient.Views;
using GmailLibrary;
using Ice;
using Dispatcher = System.Windows.Threading.Dispatcher;
using Exception = System.Exception;

namespace CareStoreMailClient.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Members

        private IMailManager _gmailManager;
        private ObservableCollection<MailHeaderViewModel> _emails = new ObservableCollection<MailHeaderViewModel>();
        private bool _loading;
        private MailClientImplementation _imp;
        private AppAdapter _adapter;
        private MainWindow _mainWindow;
        public MainWindowViewModel(MainWindow mainWindow)
        {
            ConnectToCaalhp();
            _mainWindow = mainWindow;

            string mailServer = ConfigurationManager.AppSettings["mailServer"];
            string mailAddress = ConfigurationManager.AppSettings["mailAddress"];
            string mailPassword = ConfigurationManager.AppSettings["mailPassword"];
            string mailDefaultFolder = ConfigurationManager.AppSettings["mailDefaultFolder"];
            string imageFolder = ConfigurationManager.AppSettings["imageFolder"];

            int mailPort;
            bool isPortInt = int.TryParse(ConfigurationManager.AppSettings["mailPort"], out mailPort);

            if (isPortInt)
            {
                _gmailManager = new GmailManager(mailPort, mailServer, mailAddress, mailPassword, mailDefaultFolder, imageFolder);
            }
            else
            {
                MessageBox.Show("App.config is not configured correctly. mailPort must be an integer. You entered: " +
                                ConfigurationManager.AppSettings["mailPort"]);
            }

            Emails = new ObservableCollection<MailHeaderViewModel>();

        }

        private void ConnectToCaalhp()
        {
            const string endpoint = "localhost";
            try
            {
                _imp = new MailClientImplementation(_mainWindow);
                _adapter = new AppAdapter(endpoint, _imp);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public Boolean Loading
        {
            get { return _loading; }
            private set
            {
                if (value.Equals(_loading)) return;
                _loading = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<MailHeaderViewModel> Emails
        {
            get { return _emails; }
            set { _emails = value; }
        }

        #endregion




        public void LoadData()
        {
            Loading = true;

            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
            {
                try
                {
                    var mailList = await _gmailManager.GetUnseenMailsAsync();

                    for (int i = mailList.Count - 1; i > 0; i--)
                    {
                        Emails.Add(new MailHeaderViewModel(mailList[i], _gmailManager));
                    }

                    Loading = false;
                }
                catch (Exception)
                {

                    throw;
                }
            }));
        }

        #region INotifyPropertyChanged code snippet
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }
}
