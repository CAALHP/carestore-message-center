﻿using System;
using GmailLibrary;

namespace CareStoreMailClient.ViewModels
{
    public class MailWindowViewModel
    {
        #region Members

        public bool DoesEmailContainImage { get; set; }

        private IMailMessage _mailMessage;

        public String ShortName
        {
            get
            {
                return GetShortName();
            }
        }
        public IMailMessage MailMessage
        {
            get
            {
                return _mailMessage;
            }
        }
        public String Subject
        {
            get
            {
                return _mailMessage.Subject;
            }
        }


        #endregion

        #region Constructors
        public MailWindowViewModel(IMailMessage mail)
        {
            _mailMessage = mail;
            DoesEmailContainImage = mail.ContainsAttachedImage();
        }
        #endregion

        public void SaveChanges()
        {
            // HVAD SKAL DER SKE HER?
        }

        private string GetShortName()
        {
            var strings = _mailMessage.From.Split('<');

            return strings[0];
        } 
    }
}
