﻿using System;
using System.Windows;
using System.Windows.Input;
using CareStoreMailClient.Commands;
using GmailLibrary;

namespace CareStoreMailClient.ViewModels
{
    public class MailHeaderViewModel
    {
        #region Members
        public bool DoesEmailContainImage { get; set; }
        public ICommand MailClickCommand { get; private set; }

        public IMailManager GmailManager { get; private set; }

        private IMailMessage _mailMessage;

        public String MessageText
        {
            get
            {
                return ShortName + ": " + Subject;
            }
        }

        public String ShortName
        {
            get
            {
                return GetShortName();
            }
        }

        public String Subject
        {
            get
            {
                return _mailMessage.Subject;
            }
        }

        public IMailMessage MailMessage
        {
            get
            {
                return _mailMessage;
            }
        }
        #endregion

        #region Constructors
        public MailHeaderViewModel(IMailMessage mail, IMailManager manager)
        {
            GmailManager = manager;

            _mailMessage = mail;
            MailClickCommand = new MailClickCommand(this);
            DoesEmailContainImage = mail.ContainsAttachedImage();
        }
        #endregion
        
        private string GetShortName()
        {
            var strings = _mailMessage.From.Split('<');

            return strings[0];
        }

        #region MailClickCommand method

        public void OpenMail()
        {
            MessageBox.Show("MailHeaderView box åbnet!");
        }

        #endregion
    }
}
