﻿using System;
using System.Windows;
using System.Windows.Threading;
using CareStoreMailClient.ViewModels;
using GmailLibrary;

namespace CareStoreMailClient.Views
{
    /// <summary>
    /// Interaction logic for WriteResponseWindow.xaml
    /// </summary>
    public partial class WriteResponseWindow : Window
    {
        private WriteResponseWindowViewModel _DataContext;
        private IMailMessage _msg;
        private IMailManager _gmailManager;

        public WriteResponseWindow()
        {
            InitializeComponent();
        }

        public WriteResponseWindow(IMailMessage msg, IMailManager manager)
        {
            _gmailManager = manager;
            _msg = msg;
            _DataContext = new WriteResponseWindowViewModel(msg);
            DataContext = _DataContext;
            InitializeComponent();

        }

        private void Send_Besked_Click(object sender, RoutedEventArgs e)
        {
            var fromString = _msg.From;

            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                try
                {
                    _gmailManager.ReplyWithText(_DataContext.Response,_msg);
                }
                catch (Exception)
                {

                    throw;
                }
            }));

            this.Close();
        }
    }
}
