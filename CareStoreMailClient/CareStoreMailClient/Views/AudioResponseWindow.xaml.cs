﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using AudioRecordingLibrary;
using GmailLibrary;

namespace CareStoreMailClient.Views
{
    /// <summary>
    /// Interaction logic for AudioResponseWindow.xaml
    /// </summary>
    public partial class AudioResponseWindow : Window
    {
        AudioFileMaker afm = new AudioFileMaker();
        private IMailMessage _msg;
        private IMailManager _mailManager;

        public AudioResponseWindow()
        {
            InitializeComponent();

        }

        public AudioResponseWindow(IMailMessage msg, IMailManager manager)
        {
            InitializeComponent();

            _msg = msg;
            _mailManager = manager;


            if (afm.error)
            {
                // Gives error message as no audio and/or video devices found
                MessageBoxResult result = MessageBox.Show("No Video/Audio capture devices detected.", "Live Source Sample", MessageBoxButton.OK, MessageBoxImage.Stop);
                this.Close();
                return;
            }
        }

        private void tbtn_record_Checked(object sender, RoutedEventArgs e)
        {
            afm.Start();
            (sender as ToggleButton).Content = "Stop";

            lbl_message.Content = "It is now recording. To end the recording press \"Stop\"";

        }

        private void tbtn_record_Unchecked(object sender, RoutedEventArgs e)
        {
            afm.Stop();
            (sender as ToggleButton).Content = "Start";

            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                try
                {
                    _mailManager.ReplyWithVoice(_msg, afm.Filepath);
                }
                catch (Exception)
                {

                    throw;
                }
            }));

            this.Close();

        }


    }
}
