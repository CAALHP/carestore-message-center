﻿using System.Windows;
using CareStoreMailClient.ViewModels;

namespace CareStoreMailClient.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private MainWindowViewModel _VM;

        public MainWindow()
        {
            InitializeComponent();

             _VM = new MainWindowViewModel(this);
            DataContext = _VM;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _VM.LoadData();
        }
    }
}
