﻿using System.Windows.Input;
using CareStoreMailClient.ViewModels;


namespace CareStoreMailClient.Views
{
    /// <summary>
    /// Interaction logic for MailHeaderView.xaml
    /// </summary>
    public partial class MailHeaderView
    {

        private MainWindowViewModel _mainWindowViewModel;    

        public MailHeaderView()
        {
            InitializeComponent();
            _mainWindowViewModel = this.DataContext as MainWindowViewModel;

        }

        private void MailHeaderView_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            var MailHeaderView = e.Source as MailHeaderView;
            MailWindow mailWindow = new MailWindow(MailHeaderView);

            mailWindow.Show();
        }
    }
}
