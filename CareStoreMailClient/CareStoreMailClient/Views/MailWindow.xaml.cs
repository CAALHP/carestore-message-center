﻿using System;
using System.Windows;
using System.Windows.Threading;
using CareStoreMailClient.ViewModels;

namespace CareStoreMailClient.Views
{
    /// <summary>
    /// Interaction logic for MailWindow.xaml
    /// </summary>
    public partial class MailWindow : Window
    {

        private MailHeaderViewModel _dataContext;

        public MailWindow()
        {
            InitializeComponent();
        }

        public MailWindow(MailHeaderView v)
        {
            DataContext = v.DataContext;
            _dataContext = v.DataContext as MailHeaderViewModel;
            InitializeComponent();
        }

        private void WebControl1_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (_dataContext != null)
                WebControl1.LoadHTML(_dataContext.MailMessage.HTMLMessage);
        }

        private void Btn_Tilbage_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Btn_Ja_Click(object sender, RoutedEventArgs e)
        {
            var fromString = _dataContext.MailMessage.From;
            messageBox.Visibility = Visibility.Visible;
            BGrid.Visibility = Visibility.Visible;

            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                try
                {
                    _dataContext.GmailManager.ReplyYesToMail(fromString, _dataContext.MailMessage);
                }
                catch (Exception)
                {

                    throw;
                }
            }));

            this.Close();
        }

        private void Btn_Nej_Click(object sender, RoutedEventArgs e)
        {
            var fromString = _dataContext.MailMessage.From;
            messageBox.Visibility = Visibility.Visible;
            BGrid.Visibility = Visibility.Visible;        

            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                try
                {
                    _dataContext.GmailManager.ReplyYesToMail(fromString, _dataContext.MailMessage);
                }
                catch (Exception)
                {

                    throw;
                }
            }));

            this.Close();
        }

        private void Btn_Indtal_Click(object sender, RoutedEventArgs e)
        {
            AudioResponseWindow arw = new AudioResponseWindow(_dataContext.MailMessage, _dataContext.GmailManager);
            arw.Show();
        }

        private void Btn_Skriv_Click(object sender, RoutedEventArgs e)
        {
            WriteResponseWindow responseWindow = new WriteResponseWindow(_dataContext.MailMessage, _dataContext.GmailManager);

            responseWindow.Show();
        }

        private void saveAttachedPhotos_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _dataContext.GmailManager.SavedAttachedPhotos(_dataContext.MailMessage);
                MessageBox.Show("Image saved to ImageFolder.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
