﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Permissions;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CareStoreMailClient.Views;


namespace CareStoreMailClient
{
    public class MailClientImplementation : IAppCAALHPContract
    {
        private readonly MainWindow _mainWindow;

        private IAppHostCAALHPContract _host;
        private int _processId;

        public MailClientImplementation(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
        }

        private const string AppName = "Message Center";

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(object o)
        {
            // Not subscribed to any events yet
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            Show();
        }

        private void DispatchToApp(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }

        public void Show()
        {
            DispatchToApp(() =>
            {
                //DoTheMessagePump();
                var window = Application.Current.MainWindow;
                if (!window.IsVisible)
                {
                    window.Show();
                }
                window.WindowState = WindowState.Maximized;
                window.Activate();
                window.Topmost = true;
                window.Topmost = false;
                window.Focus();
                //DoTheMessagePump();
            });
        }

        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoTheMessagePump()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }
    }
}