﻿using System;
using System.Windows.Input;
using CareStoreMailClient.ViewModels;

namespace CareStoreMailClient.Commands
{
    internal class MailClickCommand : ICommand
    {

        private MailHeaderViewModel _ViewModel;

        public MailClickCommand(MailHeaderViewModel model)
        {
            _ViewModel = model;
        }

        #region ICommand implementations

        public bool CanExecute(object parameter)
        {
            // needs to be implemented?
            return true;
        }

        public void Execute(object parameter)
        {
            _ViewModel.OpenMail();
        }

       
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        #endregion
    }
}
