﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using CAALHP.Contracts;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CareStoreMailClient.Views;

namespace MessageCenter
{
    public class MessageCenterImplementation : IAppCAALHPContract
    {

        private IAppHostCAALHPContract _host;
        private Thread _thread;
        private volatile Application _app;
        private int _processId;
        private MainWindow _currentWindow;
        private const string AppName = "MessageCenter";

        public MessageCenterImplementation()
        {
            Debugger.Launch();
            InitThread();
            while (_app == null) { }
            DispatchToApp(() =>
            {
                _currentWindow = new MainWindow();
                _currentWindow.Hide();
            });
        }

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                _app.Run();
            });

            _thread.SetApartmentState(ApartmentState.STA);
            _thread.Start();
        }

        private void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(object o)
        {
            // Not subscribed to any events yet
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            
            /* Subscribe to events */
            //_host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            
            Show();
        }

        public void Show()
        {
            DispatchToApp(() =>
            {
                _currentWindow.Show();
                _currentWindow.Activate();
            });
        }
    }
}
