﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.Live;

namespace AudioRecordingLibrary
{
    public class AudioFileMaker
    {
        /// <summary>
        /// Creates job for capture of live source
        /// </summary>
        private LiveJob job;

        public String Filepath { get; private set; }

        /// <summary>
        /// Device for live source
        /// </summary>
        public LiveDeviceSource deviceSource;

        public bool error;

        public AudioFileMaker()
        {
            // Starts new job for preview window
            job = new LiveJob();

            // Aquires audio devices

            Collection<EncoderDevice> devices = EncoderDevices.FindDevices(EncoderDeviceType.Audio);
            EncoderDevice audio = devices.Count > 0 ? devices[0] : null;
            for (int i = 1; i < devices.Count; ++i)
                devices[i].Dispose();
            devices.Clear();

            // Checks for a/v devices
            if (audio != null)
            {
                // Create a new device source. We use the first audio device on the system
                deviceSource = job.AddDeviceSource(null, audio);

                // Make this source the active one
                job.ActivateSource(deviceSource);
            }
            else
            {
                error = true;
            }
        }


        public void Start()
        {
            // Sets up publishing format for file archival type
            FileArchivePublishFormat fileOut = new FileArchivePublishFormat();
            job.ApplyPreset(LivePresets.VC1512kDSL16x9);

            // Gets timestamp and edits it for filename
            string timeStamp = DateTime.Now.ToString();
            timeStamp = timeStamp.Replace("/", "-");
            timeStamp = timeStamp.Replace(":", ".");

            // Sets file path and name
            string path = "C:\\output\\";
            string filename = "Capture" + timeStamp + ".wmv";

            Filepath = path + filename;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            fileOut.OutputFileName = Path.Combine(path, filename);

            // Adds the format to the job. You can add additional formats as well such as
            // Publishing streams or broadcasting from a port
            job.PublishFormats.Add(fileOut);

            // Start encoding
            job.StartEncoding();
        }

        public void Stop()
        {
            job.StopEncoding();
        }

        public void Dispose()
        {
            // closes devices for preview and disposes of job
            if (deviceSource != null)
            {
                job.RemoveDeviceSource(deviceSource);
                deviceSource.Dispose();
            }

            // disposes of job
            if (job != null)
                job.Dispose();
        }
    }
}
