﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ActiveUp.Net.Mail;

namespace GmailLibrary
{
    public interface IMailManager
    {
        void ReplyYesToMail(String Receiver, IMailMessage msg);
        void ReplyNoToMail(String Receiver, IMailMessage msg);
        void ReplyWithText(String responseText, IMailMessage msg);
        void ReplyWithVoice(IMailMessage msg, String filepath);
        void SavedAttachedPhotos(IMailMessage msg);
        void Connect();
        int[] GetMessageIds();
        Message GetMessageWithId(int id);
        List<MailMessage> GetUnseenMails();
        Task<List<MailMessage>> GetUnseenMailsAsync();

    }
}