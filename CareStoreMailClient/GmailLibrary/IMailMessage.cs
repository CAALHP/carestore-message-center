﻿using System;
using ActiveUp.Net.Mail;

namespace GmailLibrary
{
    public interface IMailMessage
    {
        String From { get; set; }
        String Message { get; set; }
        String Subject { get; set; }
        bool ContainsAttachedImage();
        String HTMLMessage { get; set; }
        AttachmentCollection Attachments { get; set; }
    }
}