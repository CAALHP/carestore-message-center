﻿using System;
using ActiveUp.Net.Mail;

namespace GmailLibrary
{
    public class MailMessage : IMailMessage
    {
        public String From { get; set; }
        public String Message { get; set; }
        public String Subject { get; set; }
        private Message _msg;
        
        public bool ContainsAttachedImage()
        {
            bool isAnyAttachmentsAnImage = false;
            var attachments = _msg.Attachments;

            if (attachments.Count > 0)
            {
                foreach (MimePart attachment in attachments)
                {
                    var mimepart = attachment;
                    var isMimetypeImage = mimepart.MimeType.ToLower().Contains("image");

                    if (isMimetypeImage)
                    {
                        isAnyAttachmentsAnImage = true;
                    }
                }
            }
            return isAnyAttachmentsAnImage;
        }

        public String HTMLMessage { get; set; }
        public AttachmentCollection Attachments { get; set; }

        public MailMessage(Message msg)
        {
            _msg = msg;
            From = msg.HeaderFields["From"];
            Message = msg.BodyText.Text;
            Subject = msg.HeaderFields["subject"];
            HTMLMessage = msg.BodyHtml.Text;
            Attachments = msg.Attachments;
            ContainsAttachedImage();
        }

        public MailMessage()
        {
            // For XML Serializer
        }
    }
}
