﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using ActiveUp.Net.Mail;

namespace GmailLibrary
{
    public class GmailManager : IMailManager
    {
        #region Fields, Members
        private Imap4Client _imapClient = new Imap4Client();
        private Mailbox _mailbox = new Mailbox();

        int Port { get; set; }
        String Host { get; set; }
        String Username { get; set; }
        String Password { get; set; }
        String Label { get; set; }
        public string ImagePath { get; set; }

        #endregion

        public GmailManager(int port, String hostname, String uName, String pw, String label, String imagePath)
        {
            Port = port;
            Host = hostname;
            Username = uName;
            Password = pw;
            Label = label;
            ImagePath = imagePath;
        }

        public void ReplyYesToMail(String Receiver, IMailMessage msg)
        {
            var fromAddress = new MailAddress(Username, Username);
            var toAddress = new MailAddress(Receiver, Receiver);
            string fromPassword = Password;
            string subject = "Re: " + msg.Subject;
            string body = "<div>Yes</div>" + msg.HTMLMessage;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);

            }
        }

        public void SavedAttachedPhotos(IMailMessage msg)
        {
            var attachments = msg.Attachments;

            if (!msg.ContainsAttachedImage()) return;

            foreach (MimePart attachment in attachments)
            {
                var mimepart = attachment;
                var isMimetypeImage = mimepart.MimeType.ToLower().Contains("image");

                if (isMimetypeImage)
                {
                    SavePhoto(attachment);
                }
            }
        }

        private void SavePhoto(MimePart attachment)
        {
            attachment.StoreToFile(ImagePath + attachment.Filename);
        }

        public void ReplyNoToMail(String Receiver, IMailMessage msg)
        {
            var fromAddress = new MailAddress(Username, Username);
            var toAddress = new MailAddress(Receiver, Receiver);
            string fromPassword = Password;
            string subject = "Re: " + msg.Subject;
            string body = "<div>No</div>" + msg.HTMLMessage;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);

            }
        }

        public void ReplyWithText(String responseText, IMailMessage msg)
        {
            var fromAddress = new MailAddress(Username, Username);
            var toAddress = new MailAddress(msg.From, msg.From);
            string fromPassword = Password;
            string subject = "Re: " + msg.Subject;
            string body = "<div>" + responseText + "</div><div>-----------------</div>" + msg.HTMLMessage;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);

            }
        }

        public void ReplyWithVoice(IMailMessage msg, String filepath)
        {



            var fromAddress = new MailAddress(Username, Username);
            var toAddress = new MailAddress(msg.From, msg.From);
            string fromPassword = Password;
            string subject = "Re: " + msg.Subject;
            string body = "<div>Please find attached voiceresponse.</div><div>-----------------</div>" + msg.HTMLMessage;

            // getting Data from file
            byte[] attachment = File.ReadAllBytes(filepath);

            MemoryStream ms = new MemoryStream(attachment); // Saving data to memorystream

            // Instantiating SmtpClient
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            // Instantiating MailMessage
            using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                message.Attachments.Add(new System.Net.Mail.Attachment(ms, filepath, "multipart/mixed")); // Adding Attachment to SMTP message
                smtp.Send(message);
            }
        }

        public void Connect()
        {
            _imapClient.ConnectSsl(Host, Port);
            _imapClient.Login(Username, Password);
            _imapClient.Command("capability");
            _mailbox = _imapClient.SelectMailbox(Label);
        }

        public int[] GetMessageIds()
        {
            return _mailbox.Search("ALL");
        }

        public Message GetMessageWithId(int id)
        {
            return _mailbox.Fetch.MessageObject(id);
        }

        public List<MailMessage> GetUnseenMails()
        {
            if (!_imapClient.IsConnected)
            {
                Connect();
            }

            int[] ids = GetMessageIds();

            var mails = new List<MailMessage>();

            foreach (var id in ids)
            {
                var msg = new MailMessage(GetMessageWithId(id));
                mails.Add(msg);
            }

            return mails;
        }

        public async Task<List<MailMessage>> GetUnseenMailsAsync()
        {
            return await Task.Run(() =>
            {
                if (!_imapClient.IsConnected)
                {
                    Connect();
                }

                int[] ids = GetMessageIds();

                var mails = new List<MailMessage>();

                foreach (var id in ids)
                {
                    var msg = new MailMessage(GetMessageWithId(id));
                    mails.Add(msg);
                }

                return mails;
            });
        }
    }
}
