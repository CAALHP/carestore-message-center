﻿using System;
using CAALHP.Contracts;
using CareStoreMailClient;
using Moq;

namespace MessageCenterConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var impl = new MailClientImplementation();
            var host = new Mock<IAppHostCAALHPContract>().Object;
            impl.Initialize(host,0);
            impl.Show();
            Console.ReadLine();
        }
    }
}
