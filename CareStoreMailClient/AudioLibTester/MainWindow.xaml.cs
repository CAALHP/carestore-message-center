﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AudioRecordingLibrary;

namespace AudioLibTester
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AudioFileMaker afm = new AudioFileMaker();
        
        public MainWindow()
        {
            InitializeComponent();

            

            if (afm.error)
            {
                // Gives error message as no audio and/or video devices found
                MessageBoxResult result = MessageBox.Show("No Video/Audio capture devices detected.", "Live Source Sample", MessageBoxButton.OK, MessageBoxImage.Stop);
                this.Close();
                return;
            }



        }

        private void EncodeButton_Checked(object sender, RoutedEventArgs e)
        {
            // Starts encoding
            afm.Start();
        }

        private void EncodeButton_Unchecked(object sender, RoutedEventArgs e)
        {
            // Stops encoding
            afm.Stop();
        }
    }
}
